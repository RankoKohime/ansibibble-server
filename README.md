Ansibibble-Controller-ALARM
===========================

This playbook stands up a fresh install of ~~~CentOS 8~~~ for use as an Ansible controller.

Requirements
------------

A fresh Linux server.  Presently targeted towards CentOS 8, but legacy code exists for [Arch Linux ARM](https://archlinuxarm.org/), Ubuntu, and Raspbian and **should work**.  ¯\\\_(ツ)\_/¯

Ansible 2.7 or later (May use 2.4 if you omit the reboot task at the end of the playbook)

CentOS Epel repo code comes from [GitHub - gerrlingguy/ansible-role-repo-epel](https://github.com/geerlingguy/ansible-role-repo-epel), I'm just apparently too dumb to use roles.

License
-------

GPLv3

Author Information
------------------

Ranko Kohime <ranko dot kohime at runbox dot com>
